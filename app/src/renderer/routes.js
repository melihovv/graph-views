import MainView from 'components/MainView'

export default [
  {
    path: '/',
    name: 'main-view',
    component: MainView,
  },
  {
    path: '*',
    redirect: '/',
  },
]
